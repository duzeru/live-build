#!/bin/bash
# REFERENCIAS:
# https://manpages.debian.org/stretch/live-build/lb_config.1.fr.html
# https://live-team.pages.debian.net/live-manual/html/live-manual/customizing-package-installation.en.html
# https://wiki.ubuntu.com/S390X/InstallationGuide/AutomatedInstallsWithPreseed?action=AttachFile&do=get&target=preseed_kvm.cfg
# https://www.vivaolinux.com.br/artigo/Criando-um-LiveCD-a-partir-de-uma-instalacao-do-Debian-Lenny
#https://framagit.org/dflinux/DFiso/
#
# Desenvolvido para DuZeru 4 Edição 0 DZ 4.0
# OBS: Debian 10 utiliza o padrão su - para acessar como root
# Autor: Cláudio A. Silva

#apt-get install git live-build live-manual live-config schroot -y

#git clone https://gitlab.com/duzeru/live-build.git

# CRIAR O DIRETORIO NECESSARIO PARA GERACAO DA LIVE
mkdir ~/live
cd ~/live
lb clean
lb config
echo -e '\033[01;32m Criado o diretório e preparado Skel da live \033[00;37m'
sleep 3

# INSERIR ARQUIVOS ESSENCIAIS
cd ~/live-build
cp -r ./archives/* ~/live/config/archives/
cp -r ./package-lists/* ~/live/config/package-lists/
cp -r ./includes.binary/* ~/live/config/includes.binary/
cp -r ./includes.chroot/* ~/live/config/includes.chroot/
echo -e '\033[01;32m Copiado todos os diretorios da live com sucesso\033[00;37m'
sleep 5

cd ~/live

lb config \
--mode debian \
--distribution buster \
--parent-distribution buster \
--apt-indices false \
--backports true \
--image-name dz4 \
--debian-installer-gui true \
--debian-installer live \
--archive-areas "main contrib non-free" \
--bootappend-live "boot=live components hostname=duzeru username=dz" \
--updates true \
--backports true \
--memtest none \
--debian-installer-gui true \
--debian-installer true \
#--interactive shell \

echo -e '\033[01;32m Pronto para gerar a imagem \033[00;37m'
sleep 5

lb build
