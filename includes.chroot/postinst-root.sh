#!/bin/sh

#Adicionar shell zsh no usuário, correção da criação do usuário padrão pelo Calamares
sed -i 's/bash/zsh/g' /etc/passwd

#Remover sources.listd
echo "" > /etc/apt/sources.list

# Remover os adicionais do Virtualbox caso for Máquina real
#lspci | grep -i virtualbox > /dev/null

#VM=$(echo $?)

#if test $VM = 0 ;
#then {
#   echo "É Virtualbox"
#  apt-get remove --purge -y virtualbox-guest-dkms virtualbox-guest-x11 virtualbox-guest-utils
#}
#else {
# echo "Não é Virtualbox"
#}
#fi

