#!/bin/bash

mkdir -p ~/.config/gtk-3.0
cat ~/.config/user-dirs.dirs | tail -n 7 | grep -vi publicshare | grep -vi templates | grep -vi desktop | cut -d "/" -f 2 | cut -d '"' -f 1 | sed "s/^/file:\/\/\/home\/$USER\//" > ~/.config/gtk-3.0/bookmarks

rm ~/.config/autostart/postinst-user-end.desktop
rm ~/postinst-user-end.sh
