# live-build
Customização exclusiva para a versão DZ4.0

Create image ISO for DuZeru GNU/Linux

live-build é um conjunto de scripts para criar imagens do sistema live.
A ideia por trás do live-build é um conjunto de ferramentas que usa um diretório de configuração para automatizar e personalizar completamente aspectos da construção de uma imagem live.

### Sobre este treinamento

O trabalho real de live-build é implementado nos comandos de baixo nível, Eles não são usados pelos usuários finais.
Os comandos mencionados no texto devem ser executados com privilégios de superusuário que podem ser obtidos ao se tornar o usuário root via su ou usando o sudo . Para distinguir entre comandos que podem ser executados por um usuário não privilegiado e aqueles que requerem privilégios de superusuário, os comandos são prefixados por $ ou # respectivamente. Este símbolo não faz parte do comando.


### Por que criar nosso próprio Live System?

Diversos fatores, o principal é suprir alguma necessidade pessoal, 

### O que é um Live System?

Live system é um termo para um sistema operacional que pode inicializar sem instalação em um disco rígido. Os Live Systems não alteram o(s) sistema(s) operacional(is) local(is) ou o(s) arquivo(s) já instalado(s) no disco rígido do computador, a menos que seja instruído a fazê-lo. Os sistemas ao vivo são normalmente inicializados a partir de mídia, como CDs, DVDs ou pen drives. Alguns também podem inicializar pela rede mas este está fora do escopo aqui.

Há vários sistemas live baseados no Debian disponíveis e eles estão fazendo um ótimo trabalho. Do ponto de vista do Debian, a maioria deles tem diversos aspectos totalmente diferentes do Debian e por isso não podem ser considerados Debian:

* Eles não são projetos Debian e, portanto, não possuem suporte dentro do Debian.
* Eles misturam diferentes distribuições, por exemplo, testes e instáveis .
* Eles modificam o comportamento e / ou a aparência de pacotes, desmontando-os para economizar espaço.
* Eles incluem pacotes de fora do repositório Debian.
* Eles enviam kernels customizados com patches adicionais que não fazem parte do Debian.
* Eles não estão disponíveis em diferentes tipos, por exemplo, CDs, DVDs, imagens USB-stick e netboots.

### Requisitos

Construir imagens do sistema em modo live possui pouquíssimos requisitos de sistema:

* Acesso de superusuário (raiz)
* Uma versão atualizada do live-build
* Um shell compatível com POSIX, como bash ou dash
* debootstrap

Note que usar Debian ou uma distribuição derivada do Debian não é necessária - o live-build será executado em quase qualquer distribuição com os requisitos acima, mas para melhor resultado e não demorar compilando o Live-Build, utilize o Debian para todas as etapas subsequêntes.

# Um pouco sobre as hierarquia.

Aqui veremos uma breve visão geral do processo de construção e instruções para um dos três tipos de imagem mais usados, esta é a iso-hibrid, o tipo de imagem mais versátil.
Apenas por curiosidade saiba simplificadamente os trê stipos de imagem:
* iso-hibrid - Pode ser usado em uma máquina virtual, mídia óptica ou dispositivo de armazenamento portátil USB.
* hdd - Uma imagem HDD é similar a uma imagem híbrida ISO em todos os aspectos, mas é adequada para inicializar a partir de pendrives USB, discos rígidos USB e vários outros dispositivos de armazenamento portáteis. Normalmente, uma imagem iso-hibrid pode ser utilizada para esta finalidade, mas se você tiver um BIOS que não manipule corretamente as imagens híbridas, precisará de uma imagem de HDD.
* netboot - como o próprio nome diz, uma imagem de boot pela rede.


Live System é feito das seguintes partes:

* **Imagem do kernel do Linux:** geralmente chamada vmlinuz
* **Imagem de disco RAM inicial (initrd):** um disco RAM configurado para a inicialização do Linux, contendo módulos possivelmente necessários para montar a imagem do sistema e alguns scripts para fazer isso, não o faremos manualmente, apesar de ser possível.
* **Imagem do sistema (System Image):** A imagem do sistema de arquivos do sistema operacional. Normalmente, um sistema de arquivos compactado SquashFS é usado para minimizar o tamanho da imagem do Live System. Note que é somente leitura. Assim, durante a inicialização, o sistema live utilizará um disco RAM e um mecanismo 'union' para permitir a gravação de arquivos no sistema em execução. No entanto, todas as modificações serão perdidas no encerramento.
* **Bootloader:** Um pequeno pedaço de código criado para inicializar a partir do meio escolhido, possivelmente apresentando um prompt ou menu para permitir a seleção de opções/configurações. Ele carrega o kernel do Linux e seu initrd para rodar com um sistema de arquivos do sistema associado. Diferentes soluções podem ser usadas, mas padronizando aqui vamos utilizar o GRUB (GRand Unifield Bootloader) criado pela GNU.

No final teremos resumidamente esta estrutura:

````
(Raiz do sistema)
|-------+boot
|         |-------+grub
|         |         |-------menu.lst
|         |         |-------stage2_eltorito
|         |
|         |-------vmlinuz
|         |-------initrd.gz
|
|-------+live
|         |-------filesystem.squashfs
````

Esta estrutura será guardada em uma imagem ISO 9660.
Publicado pela Organização Internacional de Normalização (ISO), o sistema de arquivos é considerado um padrão técnico internacional.
Este formato de suporte de dados de norma industrial foi concebido originalmente para especificar o volume e as estruturas de ficheiros de discos ópticos de memória só de leitura de CD e DVD.

# Instalação

Para iniciarmos a instalação é muito simples, basta apenas executar os comandos abaixo:
````
# apt-get update && \
apt-get install live-build live-manual live-config schroot
````
Após isto, você já instalou o live-build e dependências, estamos pronto para compilar nossa ISO. Simples não? Vamos conhecer o básico destes três pacotes principais por trás de toda esta mágica.
* **live-build** - Já descrito desde o inicio do treinamento.
* **live-manual** - Contém a documentação do projeto Live Systems, uma das fontes onde este treinamento foi baseado. APós instalado você pode encontra-lo em /usr/share/doc/live-manual/
* **schroot** - Utilizado mais adiante para acessar o terminal executando comandos antes da compilação da imagem ISO do nosso projeto.


# construindo uma imagem híbrida ISO

Você precisará executar as mesmas etapas básicas para criar uma imagem a cada vez. Como primeiro exemplo, crie um diretório de construção, mude para esse diretório e execute a seguinte sequência de comandos live-build para criar uma imagem híbrida ISO básica contendo um Live System padrão sem o X.org. Ele é adequado para gravação em mídia de CD ou DVD e também para cópia em um pendrive.

O nome do diretório de trabalho é totalmente sua escolha, é uma boa idéia usar um nome que ajude a identificar a imagem com a qual você está trabalhando, especialmente se você estiver trabalhando ou experimentando diferentes tipos de imagens. Neste caso, você irá construir um sistema padrão, então vamos chamá-lo, por exemplo, live-default.
````
$ mkdir live-default && cd live-default
````

Deve ser observado que lb é um alias genérico para comandos live-build. o live-build fornece as ferramentas para gerar um diretório de configuração contendo o esqueleto do sistema. A idéia por trás do live-build é ser um framework que use um diretório de configuração para automatizar completamente e personalizar todos os aspectos da construção de uma imagem Live.
Os principais comando que utilizaremos são:
* lb config : Responsável por inicializar um diretório de configuração do sistema Live.
* lb build : Responsável por iniciar uma construção do sistema Live.
* lb clean : Responsável pela remoção de partes de uma compilação do sistema Live.

Execute o comando lb config . Isso criará uma hierarquia "config/*" no diretório atual para uso por outros comandos:
````
# lb config
````

Agora que a hierarquia "config/" existe, para construir uma imagem pura, basta apenas executar o comando lb build:
````
# lb build
````

Esse processo pode demorar um pouco, dependendo da velocidade do seu computador e da sua conexão de rede. Quando estiver completo, deve haver um arquivo de imagem live-image-ARQUITETURA.hybrid.iso, pronto para uso mas somente com o necessário para rodar em modo live.
Vemos que é muito, muito simples, utilizamos apenas dois comandos e já geramos a imagem, por trás destes comandos, diveros scripts trabalharam em conjunto para nos entregar esta mágica.

### Testando uma imagem ISO com o VirtualBox

Crie uma nova máquina virtual, altere as configurações de armazenamento para usar live-image-ARQUITETURA.hybrid.iso como o dispositivo de CD/DVD e inicie a máquina.

### Limpando seu projeto
Ao construir sua imagem, perceba que foi criado diversos diretórios, entre eles há um de grande importância que é o diretório cache onde fica os pacotes baixados pelo APT.
lb clean é responsável pela limpeza depois que um sistema é construído. Remove a compilação diretórios e remove alguns outros arquivos, incluindo arquivos de estágio, e quaisquer detritos por trás de outros comandos live-build.
````
# lb clean
````

Pode ser que você precise de uma das opções deste comando futuramente, como por exemplo, limpar o diretório de cache, Vamos conhecer as principais opções deste comando:
* **lb clean** --all - Remove chroot, binary, stage, e source. O diretório de cache é mantido. Esta é a operação padrão se nenhum argumento for passado ao comando.
* **lb clean** --cache - Remove os diretórios de cache.
* **lb clean** --binary - Remove todos os arquivos caches, arquivos, diretórios e arquivos de estágios binários.
* **lb clean** --purge - remove tudo, incluindo todos os caches. O diretório de configuração é mantido.


# Gerenciando uma configuração

Este capítulo explica como gerenciar uma configuração live da criação inicial, por meio de revisões sucessivas e versões sucessivas do software live-build e da própria imagem live.
As configurações Live raramente são perfeitas na primeira tentativa.

Por todas estas razões, os scripts **auto/*** facilitarão sua vida. Eles são invólucros simples para os comandos lb config , lb build e lb clean projetados para ajudá-lo a gerenciar sua configuração. O script auto / config armazena seu comando lb config com todas as opções desejadas, o script auto / clean remove os arquivos que contêm valores de variáveis ​​de configuração, e o script auto / build mantém um build.log de cada compilação. Cada um desses scripts é executado automaticamente toda vez que você executa o correspondente lbcomando. Usando esses scripts, sua configuração é mais fácil de ler e é mantida internamente consistente de uma revisão para a próxima. Além disso, será muito mais fácil identificar e corrigir as opções que precisam ser alteradas quando você atualizar o live-build após ler a documentação atualizada.






https://live-team.pages.debian.net/live-manual/html/live-manual/overview-of-tools.en.html


# Fontes

https://live-team.pages.debian.net/live-manual/html/live-manual/about-manual.en.html

### ISO 9660
https://www.iso.org/standard/17505.html
https://www.ibm.com/support/knowledgecenter/pt/ssw_ibm_i_73/rzam4/rzam4iso9660.htm

https://www.ibm.com/developerworks/br/library/l-initrd/index.html
